# fluxcess_vm

Vagrant for creating a fluxcess VM

A fluxcess VM runs [fluxcess Admin](https://gitlab.com/fluxcess/fluxcess_admin)
And [fluxcess Tickets](https://gitlab.com/fluxcess/fluxcess_tickets)


## Prereq

`vagrant plugin install vagrant-hostsupdater`

*IMPORTANT NOTE*: Windows users please use following workaround:
https://github.com/cogitatio/vagrant-hostsupdater#windows-uac-prompt

## Start

`vagrant up`

## Stop

`vagrant halt`

## Navigate

Vagrant creates following hosts record.
 - http://php72.local - `public/`
 - http://maildev.local - proxy to local dev smtp server web ui
 - http://admin.fluxcess.local - `public/fluxcess_admin/src/public`
 - http://tickets.fluxcess.local - `public/fluxcess_tickets/src/public`

SSL also works (https://)

## Quickstart

Browse to the fluxcess Admin application and register an account. Emails in the fluxcess vagrant vm machine are all sent locally only.