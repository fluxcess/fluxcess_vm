git_repos:
  - repo: "https://gitlab.com/fluxcess/fluxcess_tickets"
    dest: "/vagrant/public/fluxcess_tickets"
  - repo: "https://gitlab.com/fluxcess/fluxcess_admin"
    dest: "/vagrant/public/fluxcess_admin"

fluxcess_db: "fluxcess"
fluxcess_config:
  - path: "/vagrant/public/fluxcess_admin/src/conf/config.local.php"
    regexp: "^\\$gs\\['DB_DB'\\] = '.*';"
    line: "$gs['DB_DB'] = '{{ fluxcess_db }}';"
  - path: "/vagrant/public/fluxcess_admin/src/conf/config.local.php"
    regexp: "^\\$gs\\['DB_USER'\\] = '.*';"
    line: "$gs['DB_USER'] = 'root';"
  - path: "/vagrant/public/fluxcess_admin/src/conf/config.local.php"
    regexp: "^\\$gs\\['DB_PW'\\] = '.*';"
    line: "$gs['DB_PW'] = 'root';"
  - path: "/vagrant/public/fluxcess_admin/src/conf/config.local.php"
    regexp: "^\\$gs\\['BASEDIR'\\] = '.*';"
    line: "$gs['BASEDIR'] = '/vagrant/public/fluxcess_admin/src/';"
  - path: "/vagrant/public/fluxcess_admin/src/conf/config.local.php"
    regexp: "^\\$gs\\['newuserregistrationallowed'\\] = .*;"
    line: "$gs['newuserregistrationallowed'] = true;"

maildev_templates:
  - src: maildev.service.j2
    dest: /etc/systemd/system/maildev.service

postfix_relayhost: localhost
postfix_relayhost_port: 1025

base_packages:
  - name: sudo
  - name: zip
  - name: unzip
  - name: mc
  - name: screen
  - name: iftop
  - name: iotop
  - name: curl
  - name: ntp
  - name: rng-tools
  - name: ipset
  - name: apt-transport-https
  - name: bash-completion
  - name: unattended-upgrades
    state: absent
  - name: tcpdump
  - name: npm

nginx_http_params:
  - log_format main '[$time_local] $remote_addr "$request" $status "$http_referer" "$http_user_agent"'
  - log_format wpmain '[$time_local] $remote_addr $upstream_cache_status - "$request" $status "$http_referer" "$http_user_agent"'
  - sendfile "on"
  - tcp_nopush "on"
  - tcp_nodelay "on"
  - keepalive_timeout "65"
  - access_log "{{nginx_log_dir}}/access.log"
  - "error_log {{nginx_log_dir}}/error.log {{nginx_error_log_level}}"
  - server_tokens off
  - types_hash_max_size 2048
  - client_max_body_size 8m
  - fastcgi_cache_path /var/cache/nginx/fastcgi levels=1:2 keys_zone=microcache:10m max_size=1024m inactive=1h

nginx_configs:
  gzip:
    - gzip on
    - gzip_vary on
    - gzip_static on
    - gzip_disable msie6
    - gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript application/javascript

nginx_sites: 
  default:
    template: nginx/default_dev.j2
    root: /vagrant/public
    php_version: 7.2
    server_name: php72.local
    default_server: true
  maildev.local:
    template: nginx/proxy.j2
    server_name: maildev.local
    proxy_pass: http://localhost:1080
    proxy_set_header:
      - Host $host
      - X-Forwarded-Proto ttps
    proxy_socket: true
  admin.fluxcess.local:
    template: nginx/fluxcess_dev.j2
    root: /vagrant/public/fluxcess_admin/src/public
    php_version: 7.2
    server_name: admin.fluxcess.local
  tickets.fluxcess.local:
    template: nginx/default_dev.j2
    root: /vagrant/public/fluxcess_tickets/src/public
    php_version: 7.2
    server_name: tickets.fluxcess.local

php_fpm_version: 7.2
php_fpm_apt_packages:
  - php{{ php_fpm_version }}-fpm
  - php{{ php_fpm_version }}-mysql
  - php{{ php_fpm_version }}-soap
  - php{{ php_fpm_version }}-intl
  - php{{ php_fpm_version }}-zip
  - php{{ php_fpm_version }}-bcmath
  - php{{ php_fpm_version }}-gd
  - php{{ php_fpm_version }}-xml
  - php{{ php_fpm_version }}-curl
  - php{{ php_fpm_version }}-opcache
  - php{{ php_fpm_version }}-mbstring
  - php{{ php_fpm_version }}-json
  - php{{ php_fpm_version }}-readline
  - php{{ php_fpm_version }}-xmlrpc
  - php{{ php_fpm_version }}-imap
php_fpm_pool_defaults:
  pm: dynamic
  pm.max_children: 40
  pm.start_servers: 5
  pm.min_spare_servers: 5
  pm.max_spare_servers: 20
php_fpm_ini:
  - option: "expose_php"
    section: "PHP"
    value: "off"
  - option: "max_execution_time"
    section: "PHP"
    value: "300"
  - option: "memory_limit"
    section: "PHP"
    value: "128M"
  - option: "post_max_size"
    section: "PHP"
    value: "16M"
  - option: "upload_max_filesize"
    section: "PHP"
    value: "2M"
  - option: "max_input_time"
    section: "PHP"
    value: "300"
  - option: "date.timezone"
    section: "PHP"
    value: "UTC"
php_fpm_pools_default:
  - name: www-data
    listen.owner: www-data
    listen.group: www-data
    user: vagrant
    group: vagrant
    listen: /var/run/php-fpm-{{ php_fpm_version }}.sock
php_fpm_pools_host: []
php_fpm_pools:
  "{{ php_fpm_pools_host + php_fpm_pools_default }}"

mysql_user_home: /home/vagrant
mysql_user_name: vagrant
mysql_user_password: root
